$(document).ready(function() {
table = $('#users').DataTable({
 "language": {
            "lengthMenu": "Mostrando _MENU_ registros",
            "zeroRecords": "No hay datos disponibles",
            "info": "Pagina _PAGE_ de _PAGES_",
            "infoEmpty": "No hay datos disponibles",
             "sSearch": "Busqueda:",
             "sLoadingRecords": "Cargando...",
                "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":     "Último",
                  "sNext":     "Siguiente",
                  "sPrevious": "Anterior"
                }
              }
});
    $('#add_clientes').click(function(){
      $('.form-horizontal')[0].reset(); 
      $('#cedula').removeAttr("disabled");
      $('#modal_form').modal('show'); 
      $('.modal-title').text('Agregar Nuevos Clientes');
      $("#guardar_usuario").attr("title","guardar");
    }); 
    //Se ocultan los div de mensajes 
    $("#success").css("display","none");
    $("#alert").css("display","none");
    
    $('#guardar_usuario').click(function (){
    if ($("#guardar_usuario").attr("title") == "editar") {
    valor = $("#cedula").val();
    url = "clientes/edit_add/"+valor;
    }else url ="clientes/save";
    event.preventDefault();
      $.ajax({
      url:url,
      type:"POST",
      dataType: 'json',
      data:$(".form-horizontal").serialize(),
        success:function(data){
          // alert(data);
          $("#success").css("display","block");
          $("#success").html(data);
          $("#success").fadeOut(4000);
          $('.form-horizontal')[0].reset(); 
        },
        error: function (jqXHR) {
          var msg = '';
          if (jqXHR.status === 0) {
          msg = 'No tienes conexión verifica la conexion.';
          } else if (jqXHR.status == 404) {
          msg = 'Pagina no encontrada.';
          } else if (jqXHR.status == 500) {
          msg = 'Error Interno del servidor [500].';
          } else {
          msg =  jqXHR.responseText;
          }
          $("#alert").css("display","block");
          $("#alert").html(msg);
          $("#alert").fadeOut(4000);
        }
      });
    });

    //$('body').on('click','#Edit_usuario',function(){
    $('.Edit_usuario').click(function(){
    $("#cedula").attr("disabled","disabled");
    id =$(this).parent().parent().children('td').html();
    $('.form-horizontal')[0].reset(); 
    $('#modal_form').modal('show'); 
    $('.modal-title').text('Editar Personas');
        $.ajax({
        url:"clientes/edit_get/"+id,
        type:"GET",
        dataType:"JSON",
          success:function(data){
          $('#cedula').val(data.cedula);
          $('#nombre').val(data.fullname);
          $('#movil').val(data.movil);
          $('#correo').val(data.correo);
          $('#sexo').val(data.sexo);
          $("#guardar_usuario").attr("title","editar")
          $("#success").html(data);    
          },
        });
    });

    //$('body').on('click','#delete_usuario',function(){
    $('.delete_usuario').click(function(){
    id =$(this).parent().parent().children('td').html();
    if(confirm('Estas seguro que deseas eliminar al usuario '+id+'?')){
    // ajax delete data to database
        $.ajax({
        url : "clientes/delete/"+id,
        type: "POST",
        dataType: "JSON",
          success: function(data){
          //if success reload ajax table
            $('#modal_form').modal('hide');
          //reload_table();
          },
          error: function (jqXHR, textStatus, errorThrown){
            alert('Error borrando la data');
          }
        });
      }
    }); 
});
