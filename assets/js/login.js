
     $(document).ready(function(){
            if ($('#alerta').text() == 0) {
                $('#alerta').css("display","none");
            }else{
                $('#alerta').css("display","block");  
                $( "#alerta" ).slideUp( 1500 ).delay( 800 ).fadeIn( 400 ); 
            }
       $('#ingresar').click(function() {
         var user =$('#user').val();
         var pass =$('#clave').val();
         var valida =$('#valida').val();
         var mensaje = null;
            console.log(user+" " +pass)
            if ( user == '') {
              mensaje = ('El campo usuario esta vacio');
             }else if (user.length <= 3) {
               mensaje = ('El valor del campo usuario es muy corto');
             }else if ( pass == '') {
               mensaje = ('El campo clave esta vacio');
             }else if ( pass.length <= 3) {
               mensaje = ('El valor del campo clave es muy corto');
             }else if ( valida == 0) {
                mensaje = ('Usuario y clave invalidos');
             }
             if (mensaje) {
                $('#prueba').html( "<div class='alert alert-warning'> <strong>Noticia: </strong>" +mensaje+"</div>" );
                $( "#prueba" ).slideUp( 1500 ).delay( 800 ).fadeIn( 400 );
                return false;
             }            
      });
    //Acción para agregar nuevos clientes
    $('.new_clientes').click(function(){
      $(".content").load("clientes/"); 
    });
    //Acción para agregar nuevos clientes
    $('.new_reserva').click(function(){
      $(".content").load("reservas/"); 
    });
});
