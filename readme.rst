###################
What is CodeIgniter
###################

CodeIgniter is an Application Development Framework - a toolkit - for people
who build web sites using PHP. Its goal is to enable you to develop projects
much faster than you could if you were writing code from scratch, by providing
a rich set of libraries for commonly needed tasks, as well as a simple
interface and logical structure to access these libraries. CodeIgniter lets
you creatively focus on your project by minimizing the amount of code needed
for a given task.

*******************
Sistema de Reservas
*******************

Al clonar este sistema tienen que cambiar el nombre de la carpeta de la aplicación 
con el nombre de "reservas" o entrar en la ruta application/config linea 26 y colocar
el nombre de su preferencia.

En la ruta application/config/routes linea 52 se define cual va a ser el controlador
por default que va a levantar la herramienta.
