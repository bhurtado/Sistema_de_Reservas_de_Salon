<script src="<?php echo base_url('assets/js/clientes.js') ?>"></script>
<button type='button' id='add_clientes' class='btn btn-primary'  data-toggle='modal'><i class='glyphicon glyphicon-plus'></i>Nuevos Clientes</button>
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          
         <h3 class='modal-title'>Nuevo Cliente</h3>
        
        </div>
    <div class="modal-body form">
    <?=validation_errors('<div id="errors" class="alert alert-danger">','</div>'); ?>
    <form class="form-horizontal" role="form" action="<?php base_url();?>cliente/save" method="POST" role="dialog">
      <div class="form-group">
          <label for="inputcedula" class="col-sm-4 control-label">Cedula</label>
          <div class="col-sm-6">
          <input type="text" name="cedula" id="cedula" class="form-control" placeholder="Ingrese su Cedula" size="8"/>
          </div>
      </div>
        <div class="form-group">
          <label for="inputnombre" class="col-sm-4 control-label">Nombre</label>
            <div class="col-sm-6">
            <input type="text" class="form-control" name ="nombre" id="nombre" placeholder="Nombre de Cliente" size="25">
            </div>
        </div>
        <div class="form-group">
            <label for="inputclave" class="col-sm-4 control-label">Movil</label>
            <div class="col-sm-6">
            <input type="text" class="form-control" id="movil" name="movil" placeholder="Numero de movil" size="15">
            </div>
        </div>
         <div class="form-group">
            <label for="inputclave" class="col-sm-4 control-label">Correo</label>
            <div class="col-sm-6">
            <input type="text" class="form-control" id="correo" name="correo" placeholder="correo electronico" size="15">
            </div>
        </div>
          <div class="form-group">
              <label for="inputrol" class="col-sm-4 control-label">Sexo</label>
              <div class="col-sm-6">
              <select class="form-control" id="sexo" name="sexo" >
              <option value="">Selecciona el sexo</option>
              <option value="M">Masculino</option>
              <option value="F">Femenino</option>
              </select>
              </div>
          </div>
    <div class="modal-footer">
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-7">
          <button type="submit" id ="guardar_usuario" class="btn btn-primary">Guardar</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
          <div id ="success" class="alert alert-success" style="margin-top: 41px;"></div>
            <div id ="alert" class="alert alert-danger" style="margin-top: 41px;"></div>
        </div>
    </div>
        
    </form>
    </div>
    </div>
  </div>
</div>
<div id="separador" style="height:15px"> </div>
<table id="users" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Cedula</th>
                    <th>Nombre</th>
                    <th>Movil</th>
                    <th>Correo</th>
                    <th>Sexo</th>           
                    <th style="width:200px">Acción</th>
                </tr>
            </thead>
            <tbody>
            <?php
                foreach ($usuarios as $i => $usuario) {
                  echo "<tr>";
	                    echo '<td>'.$usuario->cedula.'</td>';
	                    echo '<td>'.$usuario->fullname.'</td>';
	                    echo '<td>'.$usuario->movil.'</td>';
	                    echo '<td>'.$usuario->correo.'</td>';
	                    echo '<td>'.$usuario->sexo.'</td>';
	                    echo '<td>';
	                      echo '<a class="btn btn-sm btn-default Edit_usuario" title="Editar"><i class="glyphicon glyphicon-pencil"></i> Editar</a>';
	                      echo '<a class="btn btn-sm  btn-default delete_usuario" title="Borrar"><i class="glyphicon glyphicon-trash"></i> Borrar</a>';
	                    echo '</td>';
                echo "</tr>";
                }
             ?>
            </tbody>
            <tfoot>
            </tfoot>
        </table>
