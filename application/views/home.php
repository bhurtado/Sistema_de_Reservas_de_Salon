<div class="wrapper">
  <header class="main-header">
    <a href="index2.html" class="logo">
      <span class="logo-mini"><b>S</b>R</span>
      <span class="logo-lg"><b></b>Sistema de Reservas</span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
              <img src="<?php echo base_url('/assets/img/imgavatar.jpg') ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"> <?php echo $this->session->usuario; ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="<?php echo base_url('/assets/img/imgavatar.jpg') ?>" class="img-circle" alt="User Image">
                <p>
                  <?php echo $this->session->usuario; ?>
                  <!-- <small>miembro desde</small> -->
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-center">
                  <a href="<?php echo base_url("/home/logout"); ?>" class="btn btn-default btn-flat">Salir del Sistema</a>
                </div>
              </li>
            </ul>
        </li>
      </ul>
  </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('/assets/img/imgavatar.jpg') ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo "Bienvenido ".$this->session->usuario ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <ul class="sidebar-menu">
        <li class="new_clientes"><a href="#"><i class="fa fa-user"></i> <span>Nuevo Cliente</span></a></li>
        <li class="new_reserva" ><a href="#"><i class="fa fa-clone"></i> <span>Nueva Reservacion</span></a></li>
      </ul>
    </section>
  </aside>
  <div class="content-wrapper">
    <section class="content-header">
    </section>

    <section class="content">
    </section>
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      Sistema de Reservas
    </div>
    <strong>Copyright &copy; 2016 <a href="#"></a>.</strong> Todos los derechos Reservados
  </footer>
  <div class="control-sidebar-bg">
  </div>
</div>
</body>
</html>