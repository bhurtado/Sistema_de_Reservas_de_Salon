<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('login_model', '', TRUE);
        $this->load->library('session');
    }
    public function index(){
        $data['title'] = 'Sistema de Reservas';
        $this->load->view('includes/header',$data);
        $this->load->view('login');
        $this->load->view('includes/footer');
    }
    public function validar(){
        // echo "entro";
        $data= $this->login_model->get_data($this->input->post('user'),$this->input->post('clave'));
        if (count($data)){
        // $this->session->user = $this->input->post('user');
        $this->session->usuario=$data[0]['usuario'];
        redirect(base_url("/home"));
        }else
        {    
        $this->session->set_flashdata('incorrecto','Usuario no registrado en la aplicación.');
        redirect(base_url("/login"));
        }
    }
}