<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Clientes extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('clientes_model');
		$this->load->library('form_validation');
        $this->load->library('session');
	}
	public function index()
	{
        $data['usuarios'] =$this->clientes_model->listado(); 
        $this->load->view('clientes',$data);
	}
	public function save(){
	    $this->form_validation->set_rules('cedula', 'cedula', 'required|integer');
	    $this->form_validation->set_rules('nombre', 'nombre', 'required');
	    $this->form_validation->set_rules('movil', 'movil', 'required');
	    $this->form_validation->set_rules('correo', 'correo', 'required');
	    $this->form_validation->set_rules('sexo', 'sexo', 'required');

	    if ($this->form_validation->run() == FALSE){
	               echo "El formulario se encuentra vacio";  
	    }
	    else
	    {
	        if ($this->input->is_ajax_request()){
	            $datos = array(
	                "cedula" => $this->input->post("cedula"),
	                "fullname" => $this->input->post("nombre"),
	                "movil" => $this->input->post("movil"),
	                "correo" => $this->input->post("correo"),
	                "sexo" => $this->input->post("sexo")
	                );
	            if($this->clientes_model->save($datos)==true){
	                echo json_encode("Registro Guardado Satisfactoriamente");
	            }else echo "No se pudo guardar los datos";
	        }else
	            {
	                show_404();
	            }      
	    }
    }          
    public function delete ($id){
        $this->clientes_model->delete_by_id($id);
        echo json_encode(array('Borrado' =>TRUE));
    }
    public function edit_get($id){
        $data = $this->clientes_model->get_by_id($id);
        echo json_encode($data);
    }
    public function edit_add($id){
        if ($this->input->is_ajax_request()) {
            $datos = array(
	                
	                "fullname" => $this->input->post("nombre"),
	                "movil" => $this->input->post("movil"),
	                "correo" => $this->input->post("correo"),
	                "sexo" => $this->input->post("sexo")
	                );
            // echo $id;
            // print_r($datos);exit();
            $this->clientes_model->update($id,$datos);
            echo json_encode("El registro se ha actualizado Satisfactoriamente");
        }
    }   


}
/* End of file clientes.php */
/* Location: ./application/controllers/clientes.php */
 ?>