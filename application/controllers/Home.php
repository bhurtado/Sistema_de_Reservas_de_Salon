<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('session');

	}
	public function index(){
		$data['title'] = 'Sistema de Reservas';
		$this->load->view('includes/header',$data);
		$this->load->view('home');
		$this->load->view('includes/footer');
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url("/login"));
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
 ?>