<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Login_model extends CI_model{

        public $table ="usuarios";
        public function __construct(){
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function get_data($user,$pass){
                /* Obtener los elementos en especificos del select */ 
                $this->db->select('*');
                $this->db->where('usuario', $user);
                $this->db->where('password', $pass);
                $query = $this->db->get($this->table);
                return $query->result_array();
        }

        function guardar($data){
                $this->db->insert("hd_users",$data);

                if ($this->db->affected_rows() > 0) {
                        return true;
                }
                else{
                        return false;
                }
        }

}
?>