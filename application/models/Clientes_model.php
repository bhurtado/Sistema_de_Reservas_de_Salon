<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes_model extends CI_Model {
	var $table = 'clientes';

	function __construct(){
		parent::__construct();
	}

	public function listado (){
	$query = $this->db->select('*')->get($this->table);
	$this->output->enable_profiler(); //Para mostrar el los querys en la vista y ver el tiempo de ejecucion
		return $query->result();
	}

	function save($data){
		$this->db->insert($this->table,$data);
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else{
			return false;
		}
    }

    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function delete_by_id($id){
        $this->db->where('cedula', $id);
        $this->db->delete($this->table);
    }
    
    public function get_by_id($id){
        $this->db->from($this->table);
        $this->db->where('cedula',$id);
        $query = $this->db->get();
        return $query->row();
    }

    public function update ($id,$data){
    	$this->db->update($this->table, $data, array('cedula'=>$id));
		// $this->output->enable_profiler();
    	return $this->db->affected_rows();
    }
}

/* End of file Cliente_model.php */
/* Location: ./application/models/Cliente_model.php */
 ?>